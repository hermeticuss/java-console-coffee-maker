FROM maven:3.8.4-jdk-11 AS build

WORKDIR /app
COPY . /app
RUN mvn clean package

FROM openjdk:11-jre-slim

WORKDIR /app

#COPY --from=build /app/target/your-java-app.jar /app/your-java-app.jar

COPY --from=build /app/target/CoffeeMaker-1.0-SNAPSHOT.jar /app/CoffeeMaker.jar

ENTRYPOINT ["java","-jar","/app/CoffeeMaker.jar"]