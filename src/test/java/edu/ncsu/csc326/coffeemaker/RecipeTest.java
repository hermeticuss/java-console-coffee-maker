package edu.ncsu.csc326.coffeemaker;

import edu.ncsu.csc326.coffeemaker.exceptions.RecipeException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

public class RecipeTest {

    private Recipe rCap;
    private Recipe rCap2;

    @BeforeEach
    public void setupAll() throws RecipeException {
        rCap = new Recipe();
        rCap.setName("Cappuccino");
        rCap.setAmtChocolate("7");
        rCap.setAmtCoffee("5");
        rCap.setAmtMilk("4");
        rCap.setAmtSugar("3");
        rCap.setPrice("2");

        rCap2 = new Recipe();
        rCap2.setName("Cappuccino");
        rCap2.setAmtChocolate("7");
        rCap2.setAmtCoffee("5");
        rCap2.setAmtMilk("4");
        rCap2.setAmtSugar("3");
        rCap2.setPrice("2");
    }

    @Test
    void should_get_AmtChocolate() {
        assertEquals(7, rCap.getAmtChocolate());
    }

    @Test
    void should_setAmtChocolate_Normal() {
        try {
            rCap.setAmtChocolate("10");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertEquals(10, rCap.getAmtChocolate());
    }

    @Test
    void should_throwRecipeException_when_AmtChocolateIsLetter() {
        assertThrows(RecipeException.class, () -> rCap.setAmtChocolate("g"));
    }

    @Test
    void should_throwRecipeException_when_AmtChocolateIsNegative() {
        assertThrows(RecipeException.class, () -> rCap.setAmtChocolate("-10"));
    }

    @Test
    void should_get_AmtCoffee() {
        assertEquals(5, rCap.getAmtCoffee());
    }

    @Test
    void should_setAmtCoffee_Normal() {
        try {
            rCap.setAmtCoffee("15");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertEquals(15, rCap.getAmtCoffee());
    }

    @Test
    void should_throwRecipeException_when_AmtCoffeeIsLetter() {
        assertThrows(RecipeException.class, () -> rCap.setAmtCoffee("x"));
    }

    @Test
    void should_throwRecipeException_when_AmtCoffeeIsNegative() {
        assertThrows(RecipeException.class, () -> rCap.setAmtCoffee("-8")); // Should throw an RecipeException
    }


    @Test
    void should_get_AmtMilk_Normal() {
        assertEquals(4, rCap.getAmtMilk());
    }

    @Test
    void should_setAmtMilk_Normal() {
        try {
            rCap.setAmtMilk("99");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertEquals(99, rCap.getAmtMilk());
    }

    @Test
    void should_throwRecipeException_when_AmtMilkIsLetter() {
        assertThrows(RecipeException.class, () -> rCap.setAmtMilk("t"));
    }

    @Test
    void should_throwRecipeException_when_AmtMilkIsNegative() {
        assertThrows(RecipeException.class, () -> rCap.setAmtMilk("-5"));
    }

    @Test
    void should_get_AmtSugar() {
        assertEquals(3, rCap.getAmtSugar());
    }

    @Test
    void should_setAmtSugar_Normal() {
        try {
            rCap.setAmtSugar("19");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertEquals(19, rCap.getAmtSugar());
    }

    @Test
    void should_throwRecipeException_when_AmtSugarIsLetter() {
        assertThrows(RecipeException.class, () -> rCap.setAmtSugar("S"));
    }

    @Test
    void should_throwRecipeException_when_AmtSugarIsNegative() {
        assertThrows(RecipeException.class, () -> rCap.setAmtSugar("-8"));
    }

    @Test
    void should_getName() {
        assertEquals("Cappuccino", rCap.getName());
    }

    @Test
    void should_setName_Normal() {
        rCap.setName("someNewName");
        assertEquals("someNewName", rCap.getName());
    }

    @Test
    void should_getPrice() {
        assertEquals(2, rCap.getPrice());
    }

    @Test
    void should_setPrice_Normal() {
        try {
            rCap.setPrice("123");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertEquals(123, rCap.getPrice());
    }

    @Test
    void should_throwRecipeException_when_PriceIsLetter() {
        Assertions.assertThrows(RecipeException.class, () -> rCap.setPrice("S"));
    }

    @Test
    void should_throwRecipeException_when_PriceIsNegative() {
        Assertions.assertThrows(RecipeException.class, () -> rCap.setPrice("-8"));
    }

    @Test
    void should_testToString() {
        assertEquals("Cappuccino", rCap.toString());
    }

    @Test
    void should_testHashCode() {
        assertEquals(rCap.hashCode(), rCap2.hashCode());
        rCap2.setName("Name");
        assertNotEquals(rCap.hashCode(), rCap2.hashCode());
        rCap.setName("");
        assertEquals(31, rCap.hashCode());
    }

    @Test
    void should_testEquals() {
        assertAll(
                () -> assertTrue(rCap.equals(rCap2)),            //instance of the same class
                () -> assertFalse(rCap.getName().equals("")),    //name is not empty
                () -> assertFalse(rCap.equals(null)),            //not null
                () -> assertFalse(rCap2.equals("Cappuccino")));  //not an instance of object of other type
    }
}