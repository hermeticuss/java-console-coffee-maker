package edu.ncsu.csc326.coffeemaker;

import edu.ncsu.csc326.coffeemaker.exceptions.InventoryException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class CoffeeMakerTest {

    private CoffeeMaker cm;
    private Recipe r;
    private Recipe rEsp;
    private Inventory i;

    @BeforeEach
    public void setupAll() throws Exception {
        cm = new CoffeeMaker();
        r = new Recipe();
        i = new Inventory();

        rEsp = new Recipe();
        rEsp.setName("Espresso");
        rEsp.setAmtChocolate("6");
        rEsp.setAmtCoffee("8");
        rEsp.setAmtMilk("3");
        rEsp.setAmtSugar("1");
        rEsp.setPrice("3");
    }

    @Test
    void should_returnTrue_when_RecipeAdded() {
        assertTrue(cm.addRecipe(rEsp));
    }

    @Test
    void should_returnName_when_DeleteRecipe() {
        cm.addRecipe(rEsp);
        assertEquals("Espresso", cm.deleteRecipe(0));
    }

    @Test
    void should_returnRecipeName_whenEditRecipe_Normal() {
        cm.addRecipe(rEsp);
        assertAll(() -> assertNotNull(rEsp),
                () -> assertEquals("Espresso", cm.editRecipe(0, r)),
                () -> assertEquals("", r.getName()));
    }

    @Test
    void should_addInventory_Normal() {
        try {
            cm.addInventory("10", "5", "10", "5");
        } catch (InventoryException e) {
            fail("InventoryException should not be thrown");
        }
        assertAll(() -> assertEquals(25, i.getCoffee()),
                () -> assertEquals(20, i.getMilk()),
                () -> assertEquals(25, i.getSugar()),
                () -> assertEquals(20, i.getChocolate()));
    }

    @Test
    void should_throwInventoryException_when_chocolateValueIsLetter() {
        assertThrows(InventoryException.class, () -> cm.addInventory("g", "3", "2", "-5" ));
    }

    @Test
    void should_checkInventory_Normal() {
        try {
            cm.addInventory("10", "15", "25", "5");
        } catch (InventoryException e){
            fail("InventoryException should not be thrown");
        }
        assertEquals("Coffee: 25\nMilk: 30\nSugar: 40\nChocolate: 20\n", cm.checkInventory());
    }

    @Test
    void should_returnChange_when_paidMore() {
        cm.addRecipe(rEsp);
        assertEquals(10, cm.makeCoffee(0, 13));
    }

    @Test
    void should_returnAll_when_paidLess() {
        cm.addRecipe(rEsp);
        assertEquals(2, cm.makeCoffee(0, 2));
    }

    @Test
    void should_getRecipes() {
        Recipe [] recipes = new Recipe[4];
        recipes [0] = rEsp;
        cm.addRecipe(rEsp);
        assertArrayEquals(recipes, cm.getRecipes());
    }

}