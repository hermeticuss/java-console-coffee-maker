package edu.ncsu.csc326.coffeemaker;

import edu.ncsu.csc326.coffeemaker.exceptions.InventoryException;
import edu.ncsu.csc326.coffeemaker.exceptions.RecipeException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

public class InventoryTest {
    private Inventory i;
    private Recipe rCap;

    @BeforeEach
    public void setupAll() throws RecipeException {
        i = new Inventory();

        rCap = new Recipe();
        rCap.setName("Cappuccino");
        rCap.setAmtChocolate("10");
        rCap.setAmtCoffee("10");
        rCap.setAmtMilk("10");
        rCap.setAmtSugar("10");
        rCap.setPrice("10");
    }

    @Test
    public void should_getChocolate() {
        assertEquals(15, i.getChocolate());
    }

    @Test
    void should_setChocolate_Normal() {
        i.setChocolate(100);
        assertEquals(100, i.getChocolate());
        i.setChocolate(0);
        assertEquals(0, i.getChocolate());
    }

    @Test
    void should_addChocolate_Normal() {
        try {
            i.addChocolate("20");
        } catch (InventoryException e) {
            fail("InventoryException should not be thrown");
        }
        assertEquals(35, i.getChocolate());
    }

    @Test
    void should_throwInventoryException_when_chocolateValueIsLetter() {
        assertThrows(InventoryException.class, () -> i.addChocolate("g"));
    }

    @Test
    void should_throwInventoryException_when_chocolateValueIsNegative() {
        assertThrows(InventoryException.class, () -> i.addChocolate("-6"));
    }

    @Test
    void should_getCoffee() {
        assertEquals(15, i.getCoffee());
    }

    @Test
    void should_setCoffee_Normal() {
        i.setCoffee(100);
        assertEquals(100, i.getCoffee());
        i.setCoffee(0);
        assertEquals(0, i.getCoffee());
    }

    @Test
    void should_addCoffee_Normal() {
        try {
            i.addCoffee("30");
        } catch (InventoryException e) {
            fail("InventoryException should not be thrown");
        }
        assertEquals(45, i.getCoffee());
    }

    @Test
    void should_throwInventoryException_when_coffeeValueIsLetter() {
        assertThrows(InventoryException.class, () -> i.addCoffee("rw"));
    }

    @Test
    void should_throwInventoryException_when_coffeeValueIsNegative() {
        assertThrows(InventoryException.class, () -> i.addCoffee("-10"));
    }

    @Test
    void should_getMilk() {
        assertEquals(15, i.getMilk());
    }

    @Test
    void should_setMilk_Normal() {
        i.setMilk(80);
        assertEquals(80, i.getMilk());
        i.setMilk(0);
        assertEquals(0, i.getMilk());
    }

    @Test
    void should_addMilk_Normal() {
        try {
            i.addMilk("45");
        } catch (InventoryException e) {
            fail("InventoryException should not be thrown");
        }
        assertEquals(60, i.getMilk());
    }

    @Test
    void should_throwInventoryException_when_milkValueIsLetter() {
        assertThrows(InventoryException.class, () -> i.addMilk("j"));
    }

    @Test
    void should_throwInventoryException_when_milkValueIsNegative() {
        assertThrows(InventoryException.class, () -> i.addMilk("-4"));
    }

    @Test
    void should_getSugar() {
        assertEquals(15, i.getMilk());
    }


    @Test
    void should_setSugar_Normal() {
        i.setSugar(150);
        assertEquals(150, i.getSugar());
        i.setSugar(0);
        assertEquals(0, i.getSugar());
    }

    /**
     *this test case found bug
     * */
    @Test
    void should_addSugar_Normal() {
        try {
            i.addSugar("99");
        } catch (InventoryException e) {
            fail("InventoryException should not be thrown");
        }
        assertEquals(114, i.getSugar());
    }

    @Test
    void should_throwInventoryException_when_sugarValueIsLetter() {
        Assertions.assertThrows(InventoryException.class, () -> i.addSugar("s"));
    }

    @Test
    void should_throwInventoryException_when_sugarValueIsNegative() {
        Assertions.assertThrows(InventoryException.class, () -> i.addSugar("-5"));
    }

    @Test
    void should_returnFalse_when_notEnoughCoffee() {
        try {
            rCap.setAmtCoffee("16");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertAll(() -> assertFalse(i.enoughIngredients(rCap)),
                () -> assertFalse(i.useIngredients(rCap)));
    }

    @Test
    void should_returnFalse_when_notEnoughMilk() {
        try {
            rCap.setAmtMilk("16");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertAll(() -> assertFalse(i.enoughIngredients(rCap)),
                () -> assertFalse(i.useIngredients(rCap)));
    }

    @Test
    void should_returnFalse_when_notEnoughSugar() {
        try {
            rCap.setAmtSugar("16");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertAll(() -> assertFalse(i.enoughIngredients(rCap)),
                () -> assertFalse(i.useIngredients(rCap)));
    }

    @Test
    void should_returnFalse_when_notEnoughChocolate() {
        try {
            rCap.setAmtChocolate("16");
        } catch (RecipeException e) {
            fail("RecipeException should not be thrown");
        }
        assertAll(() -> assertFalse(i.enoughIngredients(rCap)),
                () -> assertFalse(i.useIngredients(rCap)));
    }

    @Test
    void should_returnTrue_when_enoughIngredients() {
        assertAll(() -> assertTrue(i.enoughIngredients(rCap)),
                () -> assertTrue(i.useIngredients(rCap)));
    }

    /**
     *this test case found bug
     * */
    @Test
    void should_useCoffee_Normal() {
        i.useIngredients(rCap);
        assertEquals(5, i.getCoffee());
    }

    @Test
    void should_useMilk_Normal() {
        i.useIngredients(rCap);
        assertEquals(5, i.getMilk());
    }

    @Test
    void should_useSugar_Normal() {
        i.useIngredients(rCap);
        assertEquals(5, i.getSugar());
    }

    @Test
    void should_useChocolate_Normal() {
        i.useIngredients(rCap);
        assertEquals(5, i.getChocolate());
    }

    @Test
    void should_testToString() {
        i.useIngredients(rCap);
        assertEquals("Coffee: 5\nMilk: 5\nSugar: 5\nChocolate: 5\n", i.toString());
    }
}