package edu.ncsu.csc326.coffeemaker;

import edu.ncsu.csc326.coffeemaker.exceptions.RecipeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RecipeBookTest {

    private Recipe[] rArray;

    private Recipe rCap;
    private Recipe rLat;
    private Recipe rEsp;
    private Recipe rIce;
    private RecipeBook rb;

    @BeforeEach
    public void setupAll() throws RecipeException {

        rb = new RecipeBook();

        rCap = new Recipe();
        rCap.setName("Cappuccino");
        rCap.setAmtChocolate("7");
        rCap.setAmtCoffee("5");
        rCap.setAmtMilk("4");
        rCap.setAmtSugar("3");
        rCap.setPrice("5");

        rLat = new Recipe();
        rLat.setName("Dark Latte");
        rLat.setAmtChocolate("4");
        rLat.setAmtCoffee("10");
        rLat.setAmtMilk("2");
        rLat.setAmtSugar("2");
        rLat.setPrice("3");

        rEsp = new Recipe();
        rEsp.setName("Espresso");
        rEsp.setAmtChocolate("6");
        rEsp.setAmtCoffee("8");
        rEsp.setAmtMilk("3");
        rEsp.setAmtSugar("1");
        rEsp.setPrice("3");

        rIce = new Recipe();
        rIce.setName("Iced");
        rIce.setAmtChocolate("2");
        rIce.setAmtCoffee("5");
        rIce.setAmtMilk("1");
        rIce.setAmtSugar("1");
        rIce.setPrice("4");
    }

    @Test
    void should_returnTrue_when_addRecipe_Normal() {
        assertAll(() -> assertTrue(rb.addRecipe(rCap)),
                () -> assertFalse(rb.addRecipe(rCap)));
    }

    @Test
    void should_check_length() {
        rArray = rb.getRecipes();
        assertEquals(4, rArray.length);
    }

    @Test
    void should_getRecipes_Normal() {
        rb.addRecipe(rCap);
        rb.addRecipe(rLat);
        rb.addRecipe(rEsp);
        rb.addRecipe(rIce);
        rArray = rb.getRecipes();
        assertAll(
                () -> assertNotNull(rArray[0]),
                () -> assertNotNull(rArray[1]),
                () -> assertNotNull(rArray[2]),
                () -> assertNotNull(rArray[3])
        );
        assertAll(
                () -> assertEquals(rCap, rArray[0]),
                () -> assertEquals(rLat, rArray[1]),
                () -> assertEquals(rEsp, rArray[2]),
                () -> assertEquals(rIce, rArray[3])
        );
        Recipe[] recipes = new Recipe[4];
        recipes[0] = rCap;
        recipes[1] = rLat;
        recipes[2] = rEsp;
        recipes[3] = rIce;
        assertArrayEquals(recipes, rArray);
    }

    @Test
    void should_returnRecipeName_when_deleteRecipe_Normal() {
        rb.addRecipe(rCap);
        rb.addRecipe(rLat);
        rb.addRecipe(rEsp);
        rb.addRecipe(rIce);
        rArray = rb.getRecipes();
        assertEquals("Cappuccino", rb.deleteRecipe(0));
        assertEquals("", rb.deleteRecipe(0));
    }

    @Test
    void should_returnNull_when_deleteRecipe_Null() {
        rArray = rb.getRecipes();
        assertNull(rb.deleteRecipe(0));
    }

    @Test
    void should_returnRecipeName_whenEditRecipe_Normal() {
        rb.addRecipe(rCap);
        rb.addRecipe(rLat);
        rb.addRecipe(rEsp);
        rb.addRecipe(rIce);
        rArray = rb.getRecipes();
        assertEquals("Cappuccino", rb.editRecipe(0, rLat));
    }

    @Test
    void should_returnRecipeName_whenEditRecipe_Null() {
        rArray = rb.getRecipes();
        assertNull(rb.editRecipe(0, rLat));
    }
}